//
//  HKManage.swift
//  consumin_healthkit
//
//  Created by MacProSlim on 3/4/19.
//  Copyright © 2019 MacProSlim. All rights reserved.
//

import Foundation
import HealthKit

class HKManage  {
    
    let store = HKHealthStore()
    let date =  Date()
    let cal = Calendar(identifier: Calendar.Identifier.gregorian)
    
    let formatter = DateFormatter()
    var startDate = ""
    var lastDate = ""
    var result = [String:Double]()
    
    
    init() {
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        authorizeHealthKit()
    }
    
    func getValues() -> String {
       let invalidJson = "Not a valid JSON"
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
        
    }
    
    //-------------------------------------- Distances ----------------------------------------
    func getDistanceWalkingRuning(completion: @escaping (Double, NSError?) -> () ) {
        
        guard let type = HKSampleType.quantityType(forIdentifier: .distanceWalkingRunning) else {
            fatalError("Something went wrong retriebing quantity type distanceWalkingRunning")
        }
        
        let someDateTime = formatter.date(from: startDate)
        let newDate = cal.startOfDay(for: someDateTime!)
        
        let lastDateTime = formatter.date(from: lastDate)
        let endDate = cal.startOfDay(for: lastDateTime!)
        
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: endDate, options: .strictEndDate)
        
        let query = HKStatisticsQuery(quantityType: type, quantitySamplePredicate: predicate, options: [.cumulativeSum]) { (query, statistics, error) in
            var value: Double = 0
            
            if error != nil {
                print("something went wrong")
            } else if let quantity = statistics?.sumQuantity() {
                value = quantity.doubleValue(for: HKUnit.meter())
            }
            DispatchQueue.main.async {
                completion(value, error as NSError?)
            }
        }
        
        
        store.execute(query)
    }
    
    func getDistanceCycling(completion: @escaping (Double, NSError?) -> () ) {
        
        let distanceCycling = HKQuantityType.quantityType(
            forIdentifier: HKQuantityTypeIdentifier.distanceCycling)
        
        let sumOption = HKStatisticsOptions.cumulativeSum
        
        
        let someDateTime = formatter.date(from: startDate)
        let newDate = cal.startOfDay(for: someDateTime!)
        
        let lastDateTime = formatter.date(from: lastDate)
        let endDate = cal.startOfDay(for: lastDateTime!)
        
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: endDate, options: .strictEndDate)
        
        let query = HKStatisticsQuery(quantityType: distanceCycling!, quantitySamplePredicate: predicate,
                                      options: sumOption)
        { (query, statistics, error) in
            var value: Double = 0
            
            if error != nil {
                print("something went wrong")
            } else if let quantity = statistics?.sumQuantity() {
                value = quantity.doubleValue(for: HKUnit.meter())
            }
            DispatchQueue.main.async {
                completion(value, error as NSError?)
            }
        }
        
        
        store.execute(query)
    }
    
    func getDistanceSteps(completion: @escaping (Double, NSError?) -> () ) {
        
        let stepCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        
        
        let someDateTime = formatter.date(from: startDate)
        let newDate = cal.startOfDay(for: someDateTime!)
        
        let lastDateTime = formatter.date(from: lastDate)
        let endDate = cal.startOfDay(for: lastDateTime!)
        
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: endDate, options: .strictStartDate)
        
        let query = HKStatisticsQuery(quantityType: stepCount!, quantitySamplePredicate: predicate, options: .cumulativeSum) { (query, result, error) in
            
            var value: Double = 0
            
            if error != nil {
                print("something went wrong")
            } else if let quantity = result?.sumQuantity() {
                value = quantity.doubleValue(for: HKUnit.count())
            }
            DispatchQueue.main.async {
                completion(value, error as NSError?)
            }
            
        }
        
        store.execute(query)
    }
    
    //--------------------------------------End Distances--------------------------------------
    
    //-------------------------------------- Calories ----------------------------------------
    func getCalories(completion: @escaping (Double, NSError?) -> () ) {
        
        let energyBurned = HKQuantityType.quantityType(
            forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)
        
        let sumOption = HKStatisticsOptions.cumulativeSum
        
        let someDateTime = formatter.date(from: startDate)
        let newDate = cal.startOfDay(for: someDateTime!)
        
        let lastDateTime = formatter.date(from: lastDate)
        let endDate = cal.startOfDay(for: lastDateTime!)
        
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: endDate, options: .strictEndDate)
        
        let query = HKStatisticsQuery(quantityType: energyBurned!, quantitySamplePredicate: predicate,
                                      options: sumOption)
        { (query, statistics, error) in
            var value: Double = 0
            
            if error != nil {
                print("something went wrong")
            } else if let quantity = statistics?.sumQuantity() {
                value = quantity.doubleValue(for: HKUnit.kilocalorie())
            }
            DispatchQueue.main.async {
                completion(value, error as NSError?)
            }
        }
        
        
        store.execute(query)
        
    }
    
    //--------------------------------------End Distances--------------------------------------
    
    func getHealthDataValueByRange ( type : HKQuantityTypeIdentifier , strUnitType : String ,StartDate : String, EndDate : String, complition: @escaping ((((String)?) -> Void)) )
    {
        if let heartRateType = HKQuantityType.quantityType(forIdentifier: type)
        {
            if (HKHealthStore.isHealthDataAvailable()  ){
                
                let sortByTime = NSSortDescriptor(key:HKSampleSortIdentifierEndDate, ascending:false)
                
                //            let timeFormatter = NSDateFormatter()
                //            timeFormatter.dateFormat = "hh:mm:ss"
                //yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                
                //                let isoDate = "2016-04-14T10:44:00+0000"
                
                let dateS = formatter.date(from:StartDate)!
                let dateE = formatter.date(from:EndDate)!
                
                //                let yesterday = Date().yesterday
                //                print(yesterday)
                //this is probably why my data is wrong
                let predicate = HKQuery.predicateForSamples(withStart: dateS, end: dateE, options: [])
                
                let query = HKSampleQuery(sampleType:heartRateType, predicate:predicate, limit:0, sortDescriptors:[sortByTime], resultsHandler:{(query, results, error) in
                    
                    guard let results = results else {
                        return
                    }
                    
                    var arrHealthValues     = [[String:Any]]()
                    
                    for quantitySample in results {
                        let quantity = (quantitySample as! HKQuantitySample).quantity
                        
                        let healthDataUnit : HKUnit
                        if (strUnitType.count > 0 ){
                            healthDataUnit = HKUnit(from: strUnitType)
                        }else{
                            healthDataUnit = HKUnit.count()
                        }
                        
                        let tempActualhealthData = "\(quantity.doubleValue(for: healthDataUnit))"
                        
                        let tempActualRecordedDate = "\(dateFormatter.string(from: quantitySample.startDate))"
                        let tempEndRecordedDate = "\(dateFormatter.string(from: quantitySample.endDate))"
                        
                        if  (tempActualhealthData.count > 0){
                            let dicHealth : [String:Any] = ["value" :tempActualhealthData , "date" :tempActualRecordedDate , "unit" : strUnitType, "endDate":tempEndRecordedDate]
                            arrHealthValues.append(dicHealth)
                        }
                    }
                    
                    if  (arrHealthValues.count > 0)
                    {
                        var result = ""
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: arrHealthValues, options: .prettyPrinted)
                            result = String(bytes: jsonData, encoding: String.Encoding.utf8) ?? "Not a valid JSON"
                        } catch {
                            complition("Not a valid JSON")
                        }
                        
                        complition( result)
                    }
                    else
                    {
                        complition(nil)
                    }
                })
                store.execute(query)
            }
        }
    }
    
    func getWorkut(completion: @escaping(Any, NSError?)->()){
        
        let workoutPredicate = HKQuery.predicateForWorkouts(with: .other)
        
        //2. Get all workouts that only came from this app.
        let sourcePredicate = HKQuery.predicateForObjects(from: .default())
        
        
        let compound = NSCompoundPredicate(andPredicateWithSubpredicates:
            [workoutPredicate, sourcePredicate])
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate,
                                              ascending: true)
        let query = HKSampleQuery(
            sampleType: .workoutType(),
            predicate: compound,
            limit: 0,
            sortDescriptors: [sortDescriptor]) { (query, samples, error) in
                DispatchQueue.main.async {
                    guard
                        let samples = samples as? [HKWorkout],
                        error == nil
                        else {
                            
                            return
                    }
                    
                    
                    
                    completion(samples, nil)
                }
        }
        
        store.execute(query)
        
    }
    
    private func authorizeHealthKit() {
        
        HealthKitSetupAssistant.authorizeHealthKit { (authorized, error) in
            
            guard authorized else {
                
                let baseMessage = "HealthKit Authorization Failed"
                
                if let error = error {
                    print("\(baseMessage). Reason: \(error.localizedDescription)")
                } else {
                    print(baseMessage)
                }
                
                return
            }
            
            print("HealthKit Successfully Authorized.")
            
            self.getDistanceWalkingRuning { (value , error) in
                self.result["walkingRuning"] = value
                
                self.getDistanceSteps { (val, error) in
                    self.result["steps"] = val
                    
                    self.getDistanceCycling { (value, error) in
                        self.result["cycling"] = value
                        
                        self.getCalories { (value, error) in
                            self.result["calories"] = value
                            
                        }
                    }
                }
            }
            
            self.getHealthDataValueByRange(type: HKQuantityTypeIdentifier.distanceWalkingRunning, strUnitType: "m", StartDate: "2019/01/01 00:00", EndDate: "2019/03/05 23:00", complition: { (value) in
                print(value!)
            })
//
           
            
           
            
           
            
        }
        
    }
    
}
