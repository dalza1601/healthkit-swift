//
//  HKBiologicalSex.swift
//  consumin_healthkit
//
//  Created by MacProSlim on 2/8/19.
//  Copyright © 2019 MacProSlim. All rights reserved.
//

import HealthKit

extension HKBiologicalSex {
    
    var stringRepresentation: String {
        switch self {
        case .notSet: return "Unknown"
        case .female: return "Female"
        case .male: return "Male"
        case .other: return "Other"
        }
    }
}
