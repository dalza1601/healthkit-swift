//
//  HKBloodType.swift
//  consumin_healthkit
//
//  Created by MacProSlim on 2/8/19.
//  Copyright © 2019 MacProSlim. All rights reserved.
//

import HealthKit

extension HKBloodType {
    
    var stringRepresentation: String {
        switch self {
        case .notSet: return "Unknown"
        case .aPositive: return "A+"
        case .aNegative: return "A-"
        case .bPositive: return "B+"
        case .bNegative: return "B-"
        case .abPositive: return "AB+"
        case .abNegative: return "AB-"
        case .oPositive: return "O+"
        case .oNegative: return "O-"
        }
    }
}
